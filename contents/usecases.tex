%\section{When is Query Rewriting useful for Data Streams?}\label{sec:usecases}
\section{Using Rewriting for Querying Streams}\label{sec:usecases}
%\subsection{Query Rewriting Use Cases}
In previous RDF stream processors, reasoning tasks are generally missing. While in some use cases reasoning may not be a key task, we have identified at least four main use cases where our query rewriting approach for RDF streams clearly enhances the query results. These cases are focused on querying streaming data items (for instance observations in the SRBench datasets), and we show how our approach can even extend the querying capabilities of currently available CEPs, DSMSs or sensor middleware. Some of these cases may also apply to traditional OBDA for static data, but we highlight the usefulness of this approach also for data streams, which has not been shown before. We summarize these cases as follows: \textit{(i) dealing with unmapped ontology terms}, \textit{(ii) obtaining the same data through different queries}, \textit{(iii) reducing queries}, and \textit{(iv) dealing with multiple views over the same data streams}. We exemplify these use cases in the following. 

\paragraph*{Dealing with unmapped ontology terms}
In ontology-based streaming data access, if the data flows through a DSMS or CEP, then all these classes and properties should be mapped to streams or event schemas. Nevertheless it is unlikely that all ontology classes and properties are mapped, e.g. a temperature stream may be mapped to \textit{oeg-sen:AirTemperatureObservation}, but not to more general classes such as  \textit{oeg-sen:TemperatureObservation} or simply \textit{ssn:Observation}, as in the following mapping:
%
\begin{align*}
\text{\texttt{\small{SELECT id FROM observations}}} \leadsto \text{ \textit{oeg-sen:AirTemperatureObservation}}(f(\mathtt{id}))
\end{align*} 
%
\begin{comment}
In that case, if the ontology TBox is not taken into account, a query such as:

1) ?observation a oeg-sen:TemperatureObservation.

will result in no answers because the term is not mapped. On the contrary, if the inclusion:  oeg-sen:AirTemperatureObservation SubClassOf oeg-sen:TemperatureObservation is considered, then the original query can be rewritten to:

1) ?observation a oeg-sen:TemperatureObservation.
2) ?observation a oeg-sen:AirTemperatureObservation.

And as q2 as a mapping, it will return answers. 
\end{comment}

Nevertheless with our query rewriting approach we can ask for any of the superclasses of \textit{oeg-sen:AirTemperatureObservation} and get a set of non-empty answers, as inferences are compiled during the query rewriting process. Moreover, we can take advantage of query compilation against the ontology to exploit other types of inferences.
For example, let's consider the following query:
%
\begin{align*}
q(obs) \leftarrow \text{\textit{ssn:observedProperty}}(obs,prop) \wedge \text{\textit{dim:Temperature}}(prop)
\end{align*}
%
It includes a non-distinguised variable that should be an instance of \textit{dim:Temperature}, for which no mappings are given. However, considering the following TBox assertion:
%
\begin{align*}
\text{\textit{oeg-sen:TemperatureObservation}} \sqsubseteq \exists \text{ \textit{ssn:observedProperty}}. \text{\textit{dim:Temperature}}
\end{align*}
%
This query subsumes \textit{oeg-sen:TemperatureObservation}, which can be answered using the existing mappings as seen above. Thus, we see that the task of defining mappings does not need to include all possible elements of the ontology. At the same time, this functionality is a clear improvement over the limited schema querying possibilities in CEPs or DSMSs. 

\paragraph*{Obtaining the same data with different queries}
For instance, let's consider a query requesting temperature observations every 100 ms.
We may ask for instances of TemperatureObservation ($q$), for instances having an observed property of type \textit{dim:Temperature} ($q'$), or ask for instances being observed by a temperature sensor ($q''$):
%
\begin{align*}
q(obs) \leftarrow & \text{\textit{oeg-sen:TemperatureObservation}}(obs) \\
q'(obs) \leftarrow & \text{\textit{ssn:observedProperty}}(obs,prop) \wedge \text{\textit{dim:Temperature}}(prop)\\
q''(obs) \leftarrow & \text{\textit{ssn:observedBy}}(obs,sensor) \wedge \text{ \textit{aws:TemperatureSensor}}(sensor)
\end{align*}
%  
While these queries may not be equivalent according to the ontology (some are subsumed by others), given a specific set of mappings, they can be rewritten to the same resulting query. This is specially interesting for CEPs and DSMSs because continuous queries are permanently running on the RSP, and the rewriting can lead to an increased sharing of results among registered queries.  %Using the ontology we can reason on these different queries and find that some of them are subsumed by others. 

\begin{comment}
For example consider this inclusion axiom in the ontology:
%
\begin{align}
oeg-sen:TemperatureObservation SubClassOf ssn:observedBy some aws:TemperatureSensor
\end{align}
%
This effectively guarantees that q1 is subsumed by q3. 

So if we ask for q3, the rewriting is (q1  union q3), while if we ask for q1, the rewriting is only q1. Finally, as we ave seen before, not all terms ma be mapped. If only oeg-sen:TemperatureObservation is mapped, then the results of both (q1 union q3) and q1 are the same. 
\end{comment}

\paragraph*{Reducing queries}

In some cases query containment may lead to a reduction of the original query. Eventual reductions may also lead to smaller (and possibly less expensive) query plans for the underlying DSMS or CEP. Without rewriting, a traditional RSP would not be able to compute such reductions. For example let's consider the following query:
%
\begin{align*}
q(obs) \leftarrow & \text{\textit{ssn:observedBy}}(obs,sensor) \wedge \text{\textit{aws:TemperatureSensor}}(sensor)\\ &\wedge 
 \text{\textit{ssn:observationResult}}(obs,result) \wedge \text{\textit{ssn:hasValue}}(obs,value) \\ & \wedge \text{\textit{qu:unit}}(value,unit) \wedge \text{\textit{dim:TemperatureUnit}}(unit)
\end{align*}
%
Now, given the following TBox assertion:
%
\begin{align*}
\exists \text{\textit{ssn:observedBy}}.\text{\textit{aws:TemperatureSensor} } \sqsubseteq & \exists \text{\textit{ssn:observationResult}} (\exists \text{\textit{ssn:hasValue}} \\ & ( \exists \text{\textit{qu:unit}}.\text{\textit{dim:TemperatureUnit}}))
\end{align*}
%
The first part of q1 is contained on the 2nd, so the query can be simplified to:
$q(obs) \leftarrow \text{\textit{ssn:observedBy}}(obs,sensor) \wedge \text{\textit{aws:TemperatureSensor}}(sensor)$.

\begin{comment}

Which in turn can be rewritten to a UCQ:

?obs ssn:observedBy [a aws:TemperatureSensor].
union
?obs oeg-sen:TemperatureObservation.
\end{comment}


\paragraph*{Dealing with multiple views over the same data streams}
An intrinsic advantage of OBDA is that a single data source can be accessed in terms of different ontologies depending on the provided mappings. In the case of streams, this is an interesting property. As the data is updated live, it is usually not feasible to materialize the streaming data into triples if a new ontology is used. For example, if instead of the SSN Ontology, a data stream is required to be accessed using the more compact SPITFIRE ontology\footnote{SPITFIRE project ontology: \url{http://spitfire-project.eu/ontology.owl}}, we only need to provide a different set of mappings. For example, instead of declaring mappings to the \textit{ssn:ObservationValue} class, we can use \textit{spt:OV} from SPITFIRE, or instead of \textit{ssn:observedBy}, we can map to \textit{spt:outOf}. Notice that we can even use a combination of both ontologies, since they are aligned.

This is a significant advantage over other RSPs that are outside of the OBDA umbrella. The ability to change the ontology entirely, only by changing the mappings comes at a relatively low price, and allows even to share continuous queries on a DSMS or CEP, for different ontological schemas, which would not be possible in other circumstances.

