\section{Background} \label{sec:background}

As discussed already in the introduction, the work presented in this paper is based fundamentally on two main areas: OBDA (ontology-based data access) and RDF stream query processing, which we briefly introduce in this section.

\subsection{Ontology-based Data Access}\label{sec:obda}

OBDA is a data access paradigm where data from one (or more) data sources can be queried in terms of a high-level ontological model, hiding from the users the internal schema and storage details of the data sources. These ontologies are normally implemented in \textit{Description Logics (DL)} languages as we will see in Section \ref{sec:soarewriting}. In general, a DL ontology or knowledge base $\ontology$ is composed of a \textit{TBox} $\mathcal{T}$ containing intensional knowledge and an \textit{ABox} $\mathcal{A}$ containing extensional knowledge~\cite{calvanese2007}.  

As opposed to traditional ontological knowledge bases, in OBDA the assertions are not stored directly in an ABox $\abox$.
Instead, the information related to the individuals is stored in a relational database $\datasource$ and
the assertions are obtained with a set of mappings $\mappings$ that relate the relational schema of the database with the TBox concepts and roles.
Both together can be abstracted as an ABox $\vabox$, in which the mappings explain how to obtain instances from the database.
A mapping assertion can be represented as $\phiit \leadsto \psiit$, where $\phiit$ is an expression in terms of the relational schema (e.g. a \sql query) and $\psiit$ is an expression in terms of the ontology TBox $\mathcal{T}$~\cite{Poggi2008}. For example the following mapping:
%
\begin{align*}
\text{\texttt{\small{SELECT id FROM hum\_sensors}}} \leadsto \text{\textit{aws:HumiditySensor}}(f(\mathtt{id}))
\end{align*}
%
establishes that instances of the \textit{aws:HumiditySensor}\footnote{The prefix \textit{aws} stands for the agriculture ontology for meteorology: \url{http://purl.oclc.org/NET/ssnx/meteo/aws}, integrated with the Semantic Sensor Network Ontology~\cite{Compton2012}.} class are constructed based on the transformation of the \texttt{id} attribute of the query on the left hand side. Notice that instances are built using a function $f$\footnote{Implementations of such functions create an instance object using the values returned from the query, usually by constructing a URI, but at this point we leave $f$ as an abstract function.}, avoiding the so-called \textit{impedance mismatch}~\cite{Poggi2008} (the data source stores \textit{values} while the ontology instances are \textit{objects}). As discussed in Section~\ref{sec:intro}, the representation of the database data as ontology instances using these mappings can follow two main approaches: materialized and virtualized.
%In this section we introduce the concepts that are relevant for the rest of the paper.
%We have seen that we start from some data source $\datasource$ and offer another data source $\view$.
%This is done because the offered data source $\view$ has additional properties and is better suited for some purpose.
%To do this we can use the materialised approach or the virtual approach.
%In the materialized approach the data source $\view$ is constructed as a regular data source and the data is stored in it.
%
In materialization, all instances of the ABox are effectively generated using the mappings, and then stored (e.g. in a triple store). %This approach is better suited when the data sources do not change often, so that the consistency between them and the materialized view is preserved. When either the volume or the velocity of the data is high, this approach may incur in unacceptable duplications of data and loading times.
In the virtual approach an ontological \textit{view} is built over the existing data sources.
This view can be queried using languages such as SPARQL, and inferencing may also be applied during the query rewriting process, expanding the capabilities of the original data source.
To this end, \textit{query rewriting} algorithms~\cite{calvanese2007} can be applied, transforming a query $q$ using an ontology TBox $\mathcal{T}$, into a query $q^\prime$, such that for every ABox $\mathcal{A}$, the set of answers that $q^\prime$ obtains from $\mathcal{A}$ is equal to the set of answers that are entailed by $q$ over $\mathcal{T}$ and $\mathcal{A}$.
The rewritten query $q^\prime$ can be expressed as a union of conjunctive queries for any logic that is FOL-reducible \cite{calvanese2007} or FO-rewritable~\cite{gottlob2011-1}.
%

In query rewriting, FOL-reducibility or FO-rewritability is an important property, and it means  that rewritten queries are first-order queries, what allows converting them to languages like SQL without using advanced features like recursion.
It has been shown that the combined complexity of satisfiability on some of these logics is polynomial, while data complexity is AC$^0$~\cite{artale2009,gottlob2011}. Therefore, existing relational databases can be used for evaluating these queries. 

As a simple example, consider the query $q(x) \leftarrow \text{\textit{ssn:Sensor}}(x)$, requesting instances of \textit{ssn:Sensor}, and the TBox axiom: $\text{\textit{aws:HumiditySensor}} \sqsubseteq \text{\textit{ssn:Sensor}}$. Using this TBox assertion, query rewriting will normally produce the union of the following  conjunctive queries:
%
\begin{align*}
q'(x) \leftarrow  & \text{\textit{ssn:Sensor}}(x) \\
q'(x) \leftarrow &  \text{\textit{aws:HumiditySensor}}(x)
\end{align*}
%
Given the mapping in the previous example, this query can be answered using the SQL query indicated in the left hand side: \texttt{\small{SELECT id FROM hum\_sensors}}.


%
\begin{comment}
In the case of providing inference capabilities with ontologies and accessing data streams we can consider two rewritings.
For the access to streams the mappings will be defined between the predicates in the ontology and the predicates in the data streams.

The inference capabilities are provided through a similar mechanism.
The ``mappings'' in this case would be defined among the predicates in the ontology, according to its axioms.
 This means the same predicates may appear on the head and the body of the mappings.
What we find in the role of these mappings is a logic program.
If this logic program contains no functional terms we can consider it as a Datalog program.
We could use this logic program in a forward chaining fashion over the data in $\datasource$ obtain a materialised $\view$ containing the results of its inferences.
In a virtual approach we can use the logic program in a backward chaining fashion to rewrite the original query.
The characteristics of this logic program depend on the expressiveness of the description logic used.
In our examples we will use $R_i$ for the name of roles and $A_j$ for the name of atomic concepts, varying $i$ and $j$.
For example an axiom of the form $\exists R_1.A_1 \subclassof A_2$ would be translated into $R_1(x, y) \land A_1(y) \rightarrow A_2(x)$.
An axiom of the form $A_1 \subclassof \exists R_1.A_2$ would be translated to $A_1(x) \rightarrow R_1(x, y) \land A_2(y)$.
This falls beyond the expressiveness of Datalog, in this case to avoid several atoms in the head of some clause we can use a Skolem function $f_{i}$, obtaining two clauses: $A_1(x) \rightarrow R_1(x, f_{i}(x))$ and $A_1(x) \rightarrow A_2(f_{i}(x))$.


One of the most interesting properties is the first-order language reducibility or first-order rewritability property.
Intuitively, query rewriting over some ontological language $\mathcal{L}$ is FOL-reducible if any union of conjunctive queries $q$ expressed over a TBox $\mathcal{T}$ in the language $\mathcal{L}$ can be rewritten into a FOL query $q_\mathcal{T}$ such that the answers for $q_\mathcal{T}$ over the data source are the same that we would expect from $q$ over $\mathcal{T}$ and the data source.
In short, FOL-reducibility means that rewritten queries are first-order queries, what allows converting them to languages like SQL without using advanced features like recursion.
It has been shown that the combined complexity of satisfiability on these logics is polynomial, while data complexity is AC$^0$ \cite{artale2009,gottlob2011}.
\end{comment}
%

The TBox can be described using different languages or logics.
Depending on their expressiveness, the query rewriting process can be more or less expensive in terms of computation (as discussed in section  \ref{sec:soa}).
A logic with special relevance in our case is $\mathcal{ELHIO}$, which we will use for the implementation of our TBoxes.
This logic is not FOL-reducible: if the ontology contains recursive definitions that introduce free variables (e.g. $\exists R.A \sqsubseteq A$) then the rewriting cannot produce a finite and complete UCQ (union of conjunctive queries).
However, the rewriting process remains tractable (\textsc{PTime}-complete \cite{perez-urbina2009}).
This logic covers (and exceeds) a good part of the expressiveness that can be covered in FOL-reducible logics, therefore by aiming at this very expressive logic we anticipate that our techniques are easily applicable to less expressive logics as well.


%In our approach therefore, completeness is not guaranteed exclusively in the case where the ontology contains one recursive definition that introduces (infinite) existential variables and the query involves this definition.

%, and this type of axioms can be used safely as far as they are not recursive.
\begin{comment}
More precisely, axioms of the form $\exists R_1.A_1 \subclassof A_2$ may imply recursion, either on one single clause or combined with other axioms.
For example, the previous axiom could appear along with the axiom $\exists R_2.A_2 \subclassof A_1$ in the same ontology, obtaining the following succession of Horn clauses:
%
\begin{align*}
\horno{A_2(x)}{R_1(x,y), A_1(y)} \\
\horno{A_1(x)}{R_2(x,y), A_2(y)} \\
\horno{A_2(x)}{R_1(x,y), R_2(y,z), A_2(z)} \\
\horno{A_1(x)}{R_2(x,y), R_1(y,z), A_1(z)} 
\end{align*}
%
After two resolution steps we can obtain two clauses that are individually recursive.
Cyclic definitions are allowed, but the inclusion of recursive definitions will prevent the unfolding from happening completely and with it the proper rewriting of the query.
%If these axioms are recursive the result of the rewriting will be a recursive datalog program, hence the loss of FOL-rewritability.
For the description of our TBoxes we will use $\mathcal{ELHIO}$.
\end{comment}

%We provide a summary of OBDA systems and the languages they support in Section~\ref{sec:soa}.
%$\mathcal{ELHIO}$ is among the most expressive logics in the state of the art in query rewriting and its expressiveness extends slightly beyond what our system can handle. 
%More precisely we do not support the rewriting of queries that in combination with the ontology would generate an infinite UCQ, since the finiteness of the unfolding is important for integration purposes.
%By targeting this very expressive logic we can test our system to its current limits and explore some of its limitations.


\subsection{Data Stream Processing} \label{sec:streams}

OBDA has traditionally focused on providing access to data in relational databases, which are different in nature, compared to data streams, understood as infinite and time-varying sequences (ordered lists) of data values~\cite{Arasu2006}. These streams can also be abstracted as more complex events whose patterns can be detected or queried, and then delivered to a subscriber if they are relevant~\cite{Cugola2011}. The potentially infinite nature of streams and the need for continuous evaluation of data over time are some of the fundamental reasons why managing streaming data differs significantly from classical static data. %Database systems deal with mostly static and stored data, and their queries retrieve the state of the data at a given point in time, as opposed to monitoring the evolution of the data over time, in streaming and event data processing.

%Highly dynamic data processing has been studied from different perspectives, which can be roughly classified into two main groups: Data Stream Processing and Complex Event Processing. In both cases, the type of data requirements differ greatly from traditional databases. For these streaming and event-based applications, normally the most recent values are more relevant than older ones. In fact, the focus of query processing is on the current observations and live-data, while historical data is often summarized, aggregated and stored for later analysis. Hence, data \textit{recency} becomes a key factor in the design of these systems, which include time as a first-class citizen in their data models and processing algorithms. 

Streams are commonly considered as unbounded sequences of sets of tuples of values continuously appended~\cite{Golab2003}, each  of which carries a time-based index (usually a sort of timestamp). The timestamp imposes a sequential order and typically indicates when the tuple has been produced, although this notion can be extended to consider the sampling, observation or arrival time. A tuple includes named attributes according to a schema, each having a given data type. Examples of systems following this idea include TelegraphCQ~\cite{Chandrasekaran2003}, Aurora~\cite{Abadi2003} and \stream~\cite{Arasu2007}. 
This model and variants of it have been used in several domains and use cases. As an example, environmental sensor observations can be abstracted as timestamped tuples, as depicted in Figure~\ref{fig:streamexample}. In this case each stream is associated to a sensor location (e.g. \textit{milford1}), and each tuple contains data about three observed properties: temperature, humidity and air pressure, encoded as \textit{temp, hum} and \textit{pres} respectively.

\begin{figure}[h]
  \centering
  \includegraphics[width=.6\textwidth]{img/streamexample}
  \vspace{5pt}
  \caption{Observations modeled as streaming timestamped tuples.}
  \label{fig:streamexample}
\end{figure}

%A formal definition for such a model is provided in the \stream system~\cite{Arasu_07}. A stream $s$ is defied as a set of elements of the form $(t,\tau)$ where $t$ is a tuple according to a given schema and $\tau$ is a non-decreasing timestamp.
%The notion of timestamp does not need to be necessarily related to the measured processing time: it is sometimes abstracted as a ``tick'' or integer-increasing value, and its importance resides in the ability of establishing an order among the stream tuples.

\paragraph*{Continuous Query Processing.}
Some of the key features in most streaming data systems are the possibility of continuous processing and the use of window operators in query languages. Streaming values are pushed by the stream source (e.g. a sensor) at possibly unknown rates and without explicit control of data arrival~\cite{Babu2001}. This concept changes the usual query execution model, since data arrival initiates query processing. In a traditional one-off database query, the result is immediately returned after query evaluation. 
Conversely, a continuous query resides in the query processor, and produces results as soon as streaming data matches the query criteria. 
Then the client may actively retrieve the data in pull mode, or the query processor may push it to clients as it becomes available or at specified time intervals.

Another feature in streaming data languages is \textit{windowing}.
The sliding window model is the most common and widespread norm for streaming data processing. The idea behind windows is simple: they limit the scope of tuples to be considered by the query operators. For instance the window $w2$ in Figure~\ref{fig:slidingwindow} has a starting point and an offset that limits its scope over certain stream tuples. A subsequent window $w1$ will bind data tuples in a latter interval of the stream. Notice that windows may immediately follow each other (\textit{tumbling} windows), overlap, or be non-contiguous in terms of time. Windows are usually specified in terms of an offset or width, and a slide that indicates the periodicity with which a new window will be created. Considering, for instance, a query that asks for the temperature average in the latest 10 minutes, a window can be set from the current time to the past 10 minutes, and all the remaining data can be excluded. However, for continuous queries, the window \textit{slides} over time, that is, for each execution time, the window boundaries will have moved. 

\begin{figure}[h]
  \centering
  \includegraphics[width=.7\textwidth]{img/slidingwindow}
  \vspace{5pt}
  \caption{Windows $w1$ and $w2$ over a data stream.}
  \label{fig:slidingwindow}
\end{figure}


%Sliding windows, tumbling windows, landmark windows, update-interval windows and other variations have been present in different systems including~\cite{Sullivan1998,Chandrasekaran_03,Arasu_07,Abadi_2003}. 


\subsection{RDF Stream Query Processing}\label{sec:rdfstream}

The streaming data query languages and systems proposed and implemented in the last years are to a large extent based on relational models and languages. 
%Therefore they share similar problems in terms of heterogeneity and lack of explicit semantics that enable advanced querying and reasoning. 
In this section we discuss the main proposals for extending SPARQL to query RDF data streams, in what has been called \textit{RDF stream processing} (RSP)\footnote{The community is currently in the process of converging on a query model for RDF streams through the W3C RSP Group: \url{http://www.w3.org/community/rsp}}.
%\rdf streams have been introduced as a suitable data model in the literature, while extensions for \sparqul have been proposed as query languages for such type of data. %We detail the \rdf stream models used by these extensions, and the syntax and semantics of the query languages.
% september2015 -- update according to RSP semantics:
\paragraph*{Stream model} A key element for the \sparqul streaming languages is the data model used to represent \rdf streams. An \rdf stream $S$ is defined as a sequence of pairs $(T,\tau)$ where $T$ is a triple  $\langle s,p,o \rangle$ and $\tau$ is a timestamp in the infinite set of monotonically non-decreasing timestamps $\Time$. Formally, we define the stream $S$ as the sequence (ordered list):
%
\begin{align*}
  S = \left\{\left\langle \left\langle s,p,o \right\rangle, \tau\right\rangle \mid \left\langle s,p,o \right\rangle \in \left(\left(I \cup B\right) \times I \times \left(I \cup B \cup L\right)  \right), \tau \in \Time\right\}
\end{align*}
%
where $I$, $B$ and $L$ are sets of \iri \!\!s, blank nodes and literals, respectively. 
Each of these pairs $(T,\tau)$ can be called a \emph{tagged triple}. The extensions presented here are based on the formalization of \cite{dellaglio2014} and \cite{Gutierrez2007} and have been used in language extensions such as \csparql \cite{Barbieri2009} and \sparqlstr \cite{Calbimonte2010}. Notice that the model of timestamped triples is a special case of the more generic one of timestamped graphs, currently discussed in the W3C RSP community\footnote{RSP-QL Semantics draft:\url{https://github.com/streamreasoning/RSP-QL}}.

An \rdf stream can be identified by an \iri, which allows referencing a particular stream of tagged triples, also called an \rdf \textit{stream graph} $\langle S,g_S \rangle$, where $S$ is an \rdf stream and $g_S$ the \iri. 
\rdf streams can be \textit{materialized} -i.e. tagged triples are physically stored in a repository- or \textit{virtual}. In the latter case the \rdf streams can be queried but their storage is not necessarily as \rdf tagged triples, but under another data model such as a relational stream or an event stream~\cite{Calbimonte2010}. This is a fact that is transparent for query users, who are typically unaware of the internal representation of the real data streams. 
%While these streams of \rdf triples can be queried through \sparqlstr, they are not materialized as \rdf triples, and are therefore \textit{virtual streams}. This is a fact that is transparent for query users, who are typically unaware of the internal representation of the real data streams. 
\begin{comment}
A similar conception of \rdf streams has been adopted by \cqels~\cite{Lephuoc2011}. An \rdf stream $S$ is a bag of elements $(s, p, o) : [t]$ , where $(s, p, o)$ is an \rdf triple and $t$ is a timestamp. 
Other works on temporal-aware \rdf extensions also provided mappings and translation methods to produce an equivalent \rdf graph~\cite{Tappolet2009,Rodriguez2009}. % provide mappings of the tagged triples model to standard \rdf. It uses named graphs to provide temporal context to triples, i.e. a named graph is given time information, and all triples contained in it are subject to these constraints. 
Nevertheless, we leave the definition of \rdf streams in an abstract form for the moment, regardless of possible concrete implementations. The semantics of queries over these stream graphs, can be specified in terms of underlying non-\rdf streaming data sources, making them \textit{virtual}, i.e. they are not stored or materialized.
\end{comment}

\paragraph*{SPARQL extensions for data streams} Streaming data extensions for \sparqul include operators such as windows, the ability to deal with \rdf streams, and  the possibility of declaring continuous queries. 
\begin{comment}
Early approaches such as \tausparql~\cite{Tappolet2009}, \tasparql \cite{Rodriguez2009} and \stsparql \cite{Koubarakis2010} 
% september2015 -- add any other?
used \rdf as a data model extended with time annotations, including temporal entailment, mostly based on the work of \cite{Gutierrez2007}. The time-related metadata in these approaches is associated to a named graph, and indexed so that time-based queries can be posed to the dataset.
However, these approaches do not target continuous query processing but focus on time-based queries provided as \sparqul extensions, compatible with traditional stored \rdf models.
Subsequent extensions, oriented towards streaming processing, introduce the concept of stream graphs to which window operators can be applied.
\end{comment}
\csparql~\cite{Barbieri2009}, \sparqlstr~\cite{Calbimonte2010} and \cqels~\cite{Lephuoc2011} incorporate this idea, although with some differences in syntax and semantics. 
\begin{comment}
Virtual \rdf streams in \sparqlstr can be continuously queried, and the stream tuples can be bound using sliding windows in the queries.  
The \sparqlstr syntax follows closely that of \sparqlii, adding window constructs for \rdf stream graphs and additional solution modifiers. % in a similar way to \csparql. 
In \sparqlstr each virtual \rdf stream graph is identified by an \iri, so that it can be used or referenced elsewhere in the query, and time windows of the form \texttt{[}$start$ TO $end$ SLIDE $slide$\texttt{]} can be applied to it.
\end{comment}
As an example, the query (in \sparqlstr syntax) in Listing~\ref{list:avgtemperature} requests the maximum temperature and the sensor that reported it in the last hour, from the stream identified as \texttt{\small{http://aemet.linkeddata.es/stream/observations.srdf}}. 

%
\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:avgtemperature,caption=Query the maximum temperature and the sensor that measured it in the last hour\, in \sparqlstr.,captionpos=b] 
PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#> 
PREFIX qu: <http://purl.oclc.org/NET/ssnx/qu/qu#>
PREFIX cf-property: <http://purl.oclc.org/NET/ssnx/cf/cf-property#>
SELECT (MAX(?temp) AS ?maxtemp) ?sensor
FROM NAMED STREAM <http://aemet.linkeddata.es/stream/observations.srdf> [NOW-1 HOURS] 
WHERE { 
  ?obs a ssn:Observation;
       ssn:observationResult ?result;
       ssn:observedProperty cf-property:air_temperature;
       ssn:observedBy ?sensor.
  ?result ssn:hasValue ?obsValue.
  ?obsValue qu:numericalValue ?temp. }
GROUP BY ?sensor  
\end{lstlisting}
%
%The triple graph patterns and other operators are applied to the bag of triples delimited by the time window, and the query can be continuously executed, so that each time it will produce a bag of results, in this case, variable bindings. As more tuples are streamed, this will produce a continuous \textit{stream of windows} or stream of sequences of results.
%
%In particular, \sparqlstr is different from the other approaches in that it provides query access to \textit{virtual} RDF Streams, as opposed to materialized RDF streams in the rest of the approaches. This is possible thanks to the rewriting using R2RML mappings, as discussed in Section~\ref{sec:intro}.

%Although \sparqlstr allows creating an ontological view over a streaming data source as in ODBA,  it does not take into account an ontology TBox, so as to consider inferences during query evaluation. Although the area of stream reasoning has spawned some results in this regard, they focused on materialization of data streams~\cite{Barbieri2010b} and ontology evolution~\cite{Ren2011,Lecue2012}, and not in querying. To the best of our knowledge none of the RDF stream query engine implementations currently provides more than SPARQL simple entailment.

%\sparqlstr also allows generating streams of triples from windows, with what are called window-to-stream operators. %These operators, namely \sprstream, \spistream and \spdstream produce a continuous stream that can be later reused or queried. They are applied after the \spselect or \spconstruct clauses, and their output is respectively a stream of variable bindings or triples. \\


\begin{comment}
\cqels implements a native \rdf stream query engine from scratch, not relying on a \dsms or \cep for managing the streams internally. The focus of this implementation is on the adaptivity of streaming query operators and their ability to efficiently combine streaming and stored data (e.g. Listing~\ref{list:cqelsexample}). %As it is seen in the example of Listing~\ref{list:cqelsexample}, \cqels is also based on the idea of adding window operators for \rdf streams handled as a special type of graphs.

\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:cqelsexample, caption=\cqels query retrieving the maximum temperature in the latest 30 minutes.] 
SELECT ?locName ?locDesc
FROM NAMED <http://deri.org/floorplan/>
WHERE {
  STREAM<http://deri.org/streams/rfid> [NOW] 
  {?person lv:detectedAt ?loc}
  GRAPH <http://deri.org/floorplan/>
  {?loc lv:name ?locName. 
   ?loc lv:desc ?locDesc }
  ?person foaf:name ''$Name$''. }
\end{lstlisting}


While the previous extensions to \sparqul were focused on windows and \rdf streams, \epsparql~\cite{Anicic2011} adopts a different perspective, oriented to complex pattern processing. Following the principles of complex event processing, \epsparql extensions include sequencing and simultaneity operators. The \texttt{SEQ} operator intuitively denotes that two graph patterns are joined if one occurs after the other. The \texttt{EQUALS} operator between two patterns indicates that they are joined if they occur at the same time. The optional version of these operators, \texttt{OPTIONALSEQ} and \texttt{EQUALSOPTIONAL}, are also provided. Although \epsparql lacks window operators, a similar behavior can be obtained by using filters with special time constraints. As an example, the query in Listing~\ref{list:epsparqlexample} obtains the companies with more than 50 percent drop in stock price, and the rating agency, if it previously downrated it.

\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:epsparqlexample, caption=\epsparql sequence pattern example query.] 
SELECT ?company ?ratingagency 
WHERE {
  ?company downratedby ?ratingagency }
OPTIONALSEQ {
  { ?company hasStockPrice ?price1 }
  SEQ { ?company hasStockPrice ?price2 } } 
FILTER (?price2 < ?price1 * 0.5)
\end{lstlisting}


Other recent approaches focused on event processing based on the Rete algorithm for pattern matching include Sparkwave~\cite{Komazec2012} and Instans~\cite{Rinne2012}. However, Sparkwave requires a fixed \rdf schema. Instans adds \sparqul update support to its features, but does not include windows on its query language because it claims that they may lead to inconsistent results (duplicates, missing triples because they fall outside of a window). This is not usually the case, as triples are usually not assumed to represent events on their own. \\

Now we briefly compare their features and main characteristics of these \sparqul extensions, summarized in Table~\ref{fig:comparesparqlstream}.
%\fixme{add comparison table, csparql, cqels, streamingsparql, epsparql}
%
\begin{table}[h]
  \centering
%  \includegraphics[width=1\textwidth]{Chapters/chapter9/figures/9.1/comparesparqlstream}
  \vspace{-20pt}
  \caption{Comparison of \sparqul extensions for streams.}
  \label{fig:comparesparqlstream}
\end{table}
%
While \tausparql and \tasparql  introduced time-aware operators as extensions for \sparqul, these were oriented towards static and stored \rdf, and not for streaming and continuous queries. \tausparql provides temporal wildcards to denote time relations, for example intervals where a triple pattern is valid, or time relationships between intervals. \tasparql provides an alternative syntax to time-based filters over a named graph that contains the temporal context. However, none of these approaches is designed for streaming query processing.

In terms of operators, \streamingsparql is limited in its lack of aggregates, compared to \csparql, \cqels and \sparqlstr. These three have also converged towards a \sparqlii-based specification, including aggregates, property paths, among others. \sparqlstr also adds window-to-stream operators, non-existing in other languages. It notably lacks triple based windows, mainly because they may incur in misleading results. A window in terms of triples (e.g. the latest 5 triples) is not as useful as a window in terms of an event or higher level abstraction (e.g. the latest 5 observations).
 %Because triples may only be meaningful in a pattern, and they may appear in a different order, windows may lead to nondeterministic results. For instance consider the following triples on a stream:
Finally, \epsparql is different from the other proposals as it includes sequence and co-occurrence pattern operators, typical of \cep, although in some cases they can be implemented using alternative constructs such as timestamp functions in \csparql. %In our work we do not consider these operators, although we can foresee their addition as an extension for our approach.

\end{comment}

\begin{comment}
\subsection{Resolution with free selection}

\fxnote{I don't think this is relevant for the present work. I agree.}

Resolution with free selection (RFS) has a special relevance for the approach taken in kyrie.
Thus we introduce here some necessary terminology to explain this algorithm and advise the reader to consult the references \cite{bachmair2001,perez-urbina2009-1} for a more detailed presentation.
For comparison purposes we can consider first binary resolution as an example: 

\[\frac{C \lor A \hspace{15pt} D \lor \neg B}{(C \lor D)\mu} \]

Where $\mu$ is the most general unifier (MGU) of atomic formulas $A$ and $B$.
We can see in this rule that two atoms are unified, one on each clause.
A selection function selects atoms in those clauses, so that for two atoms to be unified they have to be selected by that selection function.
A selection function for Horn clauses selects either the head of a clause or a non empty set of body atoms based on some criteria.
This allows to prioritize the inferences in which the selected atoms are selected.
Resolution with free selection for Horn clauses takes the form:

\[\frac{A \leftarrow B_1 \land \ldots \land (\underline{B_i}) \land \ldots \land B_n \hspace{15pt} \underline{C} \leftarrow D_1 \land \ldots \land D_n}{(A \leftarrow B_1 \land \ldots \land (D_1 \land \ldots \land D_n) \land \ldots \land B_n)\mu}\]

Where $\mu$ is the MGU of atomic formulas $B_i$ and $C$ and the underlined atoms are the ones selected and resolved.
The clause for which a body atom is selected is considered the main premise and the clause for which the head is selected is considered the side premise.

Saturation in resolution means applying the available resolution rules until no new clauses can be obtained.
Saturation in resolution with free selection has been proved to be correct and complete for Horn clauses \cite{lynch1997}.

\end{comment}