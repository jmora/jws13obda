\section{Experimentation} \label{sec:evaluation}

%This section focuses first on showing that our approach is feasible, as shown in the Morph-streams implementation.
%, showcasing the queries where the rewritten queries provide added value, compared to queries that disregard the ontology TBox. 
In this section we evaluate the performance of our query processing approach in terms of the throughput of query answers, considering different input streaming rates and simultaneous continuous queries. This is a common evaluation technique in the area of streaming data querying. Finally, we compare the throughput of the ontology-based query answering with an approach with no-rewriting. Even if they produce different results, this provides an indication of the incurred overhead.


%In our experiments we identified three main use cases where our ontology-based query answering results convenient, namely dealing with \textit{(i) unmapped ontology terms}, \textit{(ii) accessing data through different paths}, and \textit{(iii) reducing queries.} We exemplify these use cases and

\subsection{Ontologies and Queries for the Experimentation}

We have  used a subset of the SRBench~\cite{Zhang2012} benchmark dataset and queries, as well as a modified version of its ontology, extending the W3C SSN ontology~\cite{Compton2012}. 
The SRBench dataset includes weather observations from hurricanes reported in the US. Besides the SSN ontology\footnote{SSN Ontology: \url{http://purl.oclc.org/NET/ssnx/ssn#}}, we have used domain ontologies for describing weather-related concepts, namely the Climate \& Forecast vocabularies\footnote{Climate \& Forecast property ontology: \url{http://purl.oclc.org/NET/ssnx/cf/cf-property#}}. The resulting ontology network\footnote{Prefixed \texttt{oeg-sen}, provisionally available at: \url{https://github.com/oeg-upm/srbench/blob/master/src/test/resources/ontologies/sensordemo.owl}} used for our experimentation omits some axioms in order to comply with the expressiveness of $\mathcal{ELHIO}$, and we have also added domain specific classes extending the SSN Observation concept. 

\begin{figure*}[!h]
  \centering
  \includegraphics[width=.95\textwidth]{img/ssn}
  \caption{SSN Ontology combined with the Climate \& Forecast ontologies and the vocabulary of quantities.}
  \label{fig:ssn}
\end{figure*}


The SRBench queries already include some reasoning tasks, mainly including sub-classes and sub-properties. We have taken these queries and adapted them according to our extended ontology. The SSN ontology is always meant to be extended in order to be used in a specific domain of interest, and we did it accordingly. In the original set of SRBench queries, as the implementing systems do not support reasoning, they have tried to use workarounds like property paths: \texttt{rdf:type/rdfs:subClassOf*}. However this type of workarounds are limited and not applicable as most RSP systems currently lack property path implementations. In the proposed queries, we do not need such workarounds, and we include variations on the original SRBench queries that highlight the use cases presented in Section~\ref{sec:usecases}.
The full set of queries extending SRBench is available in our repository\footnote{ \url{https://github.com/oeg-upm/srbench/tree/master/src/main/resources/queries/ssn}}. 


\subsection{Evaluation of Throughput in Query Processing}

One of the key indicators in stream query processing is the throughput of a system, which can be measured in terms of the number of input tuples processed per unit of time. Some of the main factors that impact throughput are the rates at which the streams are fed, and also the number of queries loaded into the system. 
Notice that contrary to static data RDF stores, in streaming data systems the queries are \textit{continuous}, so they are registered in the system and output results as the stream elements arrive.
It is also worth pointing out that the query rewriting time, although important, is not the main concern of experiments like the ones presented here, because for continuous queries the rewriting is performed only once, so its cost is in general not critical in the long run.  
Our experimental results evaluate the query throughput for different stream input rates and number of simultaneous queries. Finally we assess the difference in terms of throughput of a non-rewritten query against a rewritten one.

First we measured how many query responses we get from a query (in push mode), per unit of time. Then we varied the rate at which the input is coming (e.g. every 100ms) and we increased the number of simultaneous queries running on the system (e.g. 5 continuous queries at the same time). For example, the SRBench query in Listing~\ref{list:sparqlstreval} asks for instances (e.g. observations) observed by some instance (e.g. a sensor). We used tumbling windows in all tests (e.g. 100ms in Listing~\ref{list:sparqlstreval}). All these tests have been performed on an Intel Core i7 1.60 GHz, 6 GB.

\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:sparqlstreval,
caption=\sparqlstr query for the throughput experimentation.,captionpos=b] 
PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>
PREFIX qu: <http://purl.oclc.org/NET/ssnx/qu/qu#>
SELECT DISTINCT ?observation   
FROM NAMED STREAM <http://cwi.nl/SRBench/observations> [NOW - 100 MS SLIDE 100 MS]
WHERE {
  ?observation ssn:observedBy ?sensor;
}
\end{lstlisting}


Figure~\ref{fig:eval1} shows measurements of the query results throughput for different numbers of simultaneous queries (1, 5, 10, 50, 100 queries). In this test the window is of 1000 ms. For lower input rates the throughput is expected to be lower as well, as there are much less data being processed.

\begin{figure}[h]
  \centering
  \includegraphics[width=.65\textwidth]{img/eval1}
  \caption{Query results throughput for 1-100 simultaneous queries, for 1000,100 and 10ms input rates. Window=1000ms. }
  \label{fig:eval1}
\end{figure}

We can see that for low input rates (1000ms) we get quasi linear throughput with respect to the number of queries. This indicates that the system scales as expected. However, as we increase the rate of the input the throughput increases as well, but we reach saturation. In fact, it is worth noting that for 10ms input, we see that the system starts to degrade for higher number of simultaneous queries. In these cases not all of the input data has been processed, as the system is unable to cope with that volume in that period of time.

For the next test, we also varied the window size. In Figure~\ref{fig:eval2} the tumbling window is set to 100ms, so we get 10x more answers per unit of time. The situation is similar to the previous experiment. As we reduce the window size the system needs to process results at shorter intervals, so that it reaches saturation faster, although it initially processes a larger amount of data per unit of time. Nevertheless, the system is still able to deliver results, although the volume of query results per unit of time is too high when the input is too fast. Conversely, for the input rate of 1s, the system is still far from saturation, even when there are up to 100 queries continuously producing results. 

\begin{figure}[h]
  \centering
  \includegraphics[width=.65\textwidth]{img/eval2}
  \caption{Query results throughput for 1-100 simultaneous queries, for 1000,100 and 10ms input rates. Window=100ms. }
  \label{fig:eval2}
\end{figure}

Our final experiment compares our system working in two modes: with and without query rewriting enabled.
Figure~\ref{fig:eval3} shows the results of measuring throughput, for an input of 100ms and 1-150 simultaneous queries. The rewritten query results in a union of three Esper queries, while the non-rewritten query results in a single Esper query. We  measured results in terms of throughput again. Logically the non-rewritten query returns less results (as it is rewritten to only 1 query and not a union), thus is expected to have lower throughput, but it degrades slower than the rewritten union of queries. In fact it progresses almost linearly and starts to decay after 100 simultaneous queries, approximately. At this point we see that the system has reached saturation for both the reasoning and non-reasoning cases. 
%
\begin{figure}[h]
  \centering
  \includegraphics[width=.65\textwidth]{img/eval3}
  \caption{Query results throughput for 1-150 simultaneous queries, for 100ms input rates. Window=100ms. }
  \label{fig:eval3}
\end{figure}
%
In summary this indicates that although rewriting may incur in overhead (leading to earlier degradation of the system), this is verified mostly for very high input rates. This set of experiments shows the applicability of rewriting for data streams of low to medium rates, using queries and data from the SRBench stream benchmark. 
