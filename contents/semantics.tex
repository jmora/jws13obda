\subsection{OBDA Semantics for Querying Streams} \label{sec:semantics}


We describe in this section the semantics of OBDA query answering for data streams.
An ontology $\ontology$ is composed of a TBox $\tbox$ and an ABox $\abox$, i.e. $
  \ontology = \langle \tbox, \abox \rangle
  $.
In an OBDA system the ontology $
  \OBDA = \langle \tbox, \vabox \rangle
  $ also consists of a TBox $\tbox$, but the ABox $\vabox$ is virtual, in the sense that it abstracts the details of the data source $\datasource$ through a set of mappings  $\mappings$, i.e. $\vabox = \langle \mappings, \datasource \rangle $.
This abstraction allows accessing the data in the data source $\datasource$ as instances of the virtual ABox $\vabox$ of the ontology, in a way that is transparent to the user.
Due to the inference capabilities provided by the TBox, the queries that are posed to the system obtain the so-called \textit{certain answers}, which intuitively are the kind of answers that users expect from an ontology, i.e. the answers explicitly stated in the database and those entailed by the ontology.

In our approach we constrain the queries posed to the system to \textit{conjunctive queries} (CQ) of the form:
\begin{align*}
  \qh(\vec{x}) \leftarrow p_1(\vec{x_1}) \land \ldots \land p_n(\vec{x_n})
\end{align*}
  where $\qh$ is the query head predicate, %(which does not appear in $\OBDA$)
$\vec{x}$ is a tuple of distinguished variables, $\vec{x_1} \ldots \vec{x_n}$ are tuples of variables or constants, and $p_i(\vec{x_i})$ are unary or binary atoms in the body of the query. Every variable in the head of the query ($x_j \in \vec{x}$) also appears in the body of the query. % ($\exists p_i(\vec{x_i}) \in \query. x_j \in \vec{x_i}$).
We are interested in obtaining the certain answers for this type of queries, which can be formally defined as the set:
\begin{align*}
  \cert{\query}{\OBDA} = \{ \answer \mid \query \cup \tbox \cup \vabox \models \qh(\answer) \}
\end{align*}
where $\qh$ is the head predicate of the query $\query$ over an OBDA ontology $\OBDA = \langle \tbox, \vabox \rangle$.

The purpose of query rewriting is to use the TBox $\tbox$ to rewrite the query $\query$ into a new query $\query^\prime$ such that:
\begin{align*}
  \cert{\query}{\OBDA} = \{ \answer \mid \query^\prime \cup \vabox \models \qh(\answer) \}
\end{align*} 
In our work, the new query $\query^\prime$ is a \textit{union of conjunctive queries} (UCQ), which is a set of CQ with the same head. In the following, we will describe: (i) the query rewriting semantics to obtain the UCQ $q^\prime$, (ii) the adaptation of this semantics to a continuous evaluation, and (iii) the semantics of the \sparqlstr window evaluation and query transformation using mappings.

%These types of queries are meant to be executed by other systems (e.g. a Datalog engine) or translated to other types of queries (e.g. SQL).

\subsubsection{Query Rewriting Semantics}

Our query rewriting module uses a non-recursive $\mathcal{ELHIO}$ ontology to rewrite the conjunctive query obtained in the previous step into a UCQ.
The ontology is converted to first order logic according to the rules described in~\cite{perez-urbina2010}.
Through a series of inference steps described in \cite{mora2013} the conjunctive query $\query$ is rewritten to a UCQ $\query^\prime$ such that $
  \cert{\query}{\OBDA} = \{ \answer \mid \query^\prime \cup \vabox \models \qh(\answer) \}
	$.
This UCQ is then converted into a union of BGPs (basic graph patterns) according to the BGP transformation.
Since the conversion is merely syntactical the semantics of the rules in $\mathcal{ELHIO}$ can be expressed over transformations on triple patterns (Table~\ref{tab:rdfelhio}). %The rules are there expressed in the usual form antecedent-consequent, even though {\kyrie} works in a backward-chaining fashion, starting from the query.


\newcommand{\rulerow}[3]{#1&\code{#2 }$\rightarrow$&\hspace{-5pt}\code{#3} \\ }
\begin{table}[!h]
\centering
\scalebox{0.85}{
\begin{tabular}{c r l}
\rulerow{$\mathcal{ELHIO}$ axiom}{antecedent}{consequent}
\rulerow{$A \subclassof \{a\}$}{?x {\type} ex:A}{?x = ex:a}
\rulerow{$A1 \subclassof A2$}{?x {\type} ex:A1}{?x {\type} ex:A2}
\rulerow{$A1 \sqcap A2 \subclassof A3$}{?x {\type} ex:A1 ; {\type} ex:A2}{?x {\type} ex:A3}
\rulerow{$A \subclassof \exists P$}{?x {\type} ex:A}{?x ex:P \_:fx}
\rulerow{$A1 \subclassof \exists P.A2$}{?x {\type} ex:A1}{?x ex:P [{\type} ex:A1]}
\rulerow{$A \subclassof \exists P^-$}{?x {\type} ex:A}{\_:fx ex:P ?x}
\rulerow{$A1 \subclassof \exists P^-.A2$}{?x {\type} ex:A1}{[ex:P ?x ; {\type} ex:A2]}
\rulerow{$\exists P \subclassof A$}{?x ex:P ?y}{?x {\type} ex:A}
\rulerow{$\exists P.A1 \subclassof A2$}{?x ex:P [{\type} ex:A1]}{?x {\type} ex:A2}
\rulerow{$\exists P^- \subclassof A$}{?y ex:P ?x}{?x {\type} A}
\rulerow{$\exists P^-.A1 \subclassof A2$}{[ex:P ?x ; {\type} A1]}{?x {\type} A2}
\rulerow{$P \subclassof S, P^- \subclassof S^-$}{?x ex:P ?y}{?x ex:S ?y}
\rulerow{$P \subclassof S^-, P^- \subclassof S$}{?x ex:P ?y}{?y ex:S ?x}
\end{tabular}
}
\caption{$\mathcal{ELHIO}$ axioms expressed as rules over RDF triples. Syntactically converted from Table 2 in a previous paper by Urbina et al.~\ref{perez-urbina2009-1}.}
\label{tab:rdfelhio}
\end{table}

\subsubsection{Query Rewriting for Continuous Queries }
% this paragraph should be checked according to the latest RDF stream processing semantics (AFAIK there are no implications) -- september2015
As discussed in Section~\ref{sec:background}, in OBDA the set of mappings $\mappings$ from the datasource schema to the TBox is given by expressions of the type $\Phi \leadsto \Psi$.
Then it is straightforward to transform the UCQ $\query^\prime$ into a query in terms of the original (typically relational) schema.
% However in our approach this changes slightly as the queries are in fact continuously executed, not over a static database but over dynamic streams.
In this scenario, the data source $\datasource$ is composed of infinite streams, which are mapped to $\tbox$ through the mappings $\mappings$.
These mappings allow for querying a sequence of ABoxes over time: $\vabox(0),\vabox(1),...,\vabox(t_i),...$ where $t_i$ represents a time point (as an integer).
For a given time $t$ we call $\vabox(t)$ the \textit{instantaneous ABox} at that time point.
An instantaneous ABox results from the traditional application of the OBDA mappings to a set of tuples.
The difference in this case resides in a previous step in which these tuples are grouped by their corresponding time point.
Each group of tuples corresponds to an instantaneous ABox according to the mappings.
Given two time points $s$ and $e$ that denote the start and end of a time window $w_k$  we define $\vabox_{w_k}$ the ABox consisting of the union of the instantaneous ABoxes $\vabox(t)$ such that $s \leq t < e$. 

To deal with the continuous streams, the queries are continuously executed, and include time-based window definitions to limit the number of instances of the stream. A slide parameter indicates how often this window is computed, but without loss of generality we can assume that the windows are tumbling, i.e. the window size is equal to the slide. We can represent each query as $q_{w}$ where $w$ is a window of size $\delta$. Then, if we assume $t_0$ as the initial evaluation time, $q_{w}$ will be evaluated at times $t_0,t_0+\delta,...,t_0+k\delta,...$, with $k \in \mathbb{N}$. 
%We define $q_{w_k}$ as the query for the $k$-th window. 
Then, the certain answers for this query, for the $k$-th window can be defined as:
\begin{align*}   
  \cert{q_{w}}{\langle \tbox,\vabox_{w_k} \rangle} = \{ \answer \mid q_{w} \cup \tbox \cup \vabox_{w_k} \models \qh(\answer) \}
\end{align*}
where the start and end times of the $k$-th window $w_k$ define the contents of the Abox $\vabox_{w_k}$. This window-based semantics of the certain answers is compatible with the definition given above, as we are referring to instantaneous snapshots of the ABox stream. 

Now, the same principles used in query rewriting for static data sources can be used to compute the inferences on the query, using the TBox to compute a rewritten UCQ. So, for the $k$-th window, the corresponding query $q_{w}$ will be rewritten into a new query $q^\prime_{w}$ such that:
\begin{align*}
  \cert{q_{w}}{\langle \tbox,\vabox_{w_k} \rangle} = \{ \answer \mid q^\prime_{w} \cup \vabox{w_k} \models \qh(\answer) \}
\end{align*}


This continuous query $q^\prime$ with time windows, can be concretely implemented using \sparqlstr, which as we have seen in Section~\ref{sec:rdfstream}, includes such features. Of course, a query rewriting system will not rewrite the original query $q$ for every window evaluation. Assuming that $\tbox$ does not change, the query rewriting can be executed only once. In the next section we will describe the \sparqlstr semantics, which are based on the translation of the \sparqlstr query using the mapping information.
  

\begin{comment}

When considering SPARQL the mappings $\mappings$ are used to translate $\query^\prime$ into a new $\query^{\second}$ such that $
  \cert{\query}{\OBDA} = \{ \answer \mid \query^{\second} \cup \datasource \models \rawanswer \land \translate{\mappings}{\rawanswer} = \answer\}
  $, where $\translate{\mappings}{\rawanswer}$ uses the mappings $\mappings$ to perform a translation of the answers $\rawanswer$ for $\query^{\second}$ to obtain the answers for the original query $\query$.
In our case we consider {\sparqlstr}.
This means that the data source $\datasource$ is a stream $\datastream$ which is potentially infinite.
To deal with this problem the queries allow the definition of a window $\window{\datastream}$ and so we have windowed queries $\wquery$.
In a windowed query the answers are constrained to the boundaries defined by the window.
These boundaries are specified over the query and define the limits of the data source from where the answers will be obtained.
% In this sense $\cert{\wquery}{\OBDA} = \cert{\query}{\window{\OBDA}}$.
With this boundaries, (1) a query $\query$ for which a window is specified $\window{\query}$ posed on an unconstrained OBDA system $\OBDA$ and (2) the same query without any window definition $\query$ posed over a OBDA system limited to a window $\window{\OBDA}$ are meant to obtain the same answers $\cert{\window{\query}}{\OBDA} = \cert{\query}{\window{\OBDA}}$.

The limits defined by the window will affect more precisely the extension of the OBDA system $\OBDA$, i.e. the ABox $\abox$.
Once we have introduced the concepts related with windows we can revisit the previous definitions obtaining the following ones:

$$ \cert{\wquery}{\OBDA} = \{ \answer \mid \query \cup \tbox \cup \window{\OBDAA} \models \qh(\answer) \} $$
$$ \cert{\wquery}{\OBDA} = \{ \answer \mid \query^\prime \cup \window{\OBDAA} \models \qh(\answer) \} $$
$$ \cert{\wquery}{\OBDA} = \{ \answer \mid \query^{\second} \cup \window{\datastream} \models \rawanswer \land \translate{\mappings}{\rawanswer} = \answer\} $$

From where we can see the relations between $\wquery$, the part of it without the window ($\query$) and the rewritings ($\query^\prime$ and $\query^\second$).


In our system the query $\query$ is extracted from the basic graph pattern (BGP) of a conjunctive query in \sparqlstr.
This means that to rewrite a single \sparqlstr query several calls to the rewriting in {\kyrie} may be required.
Query rewriting uses this query $\query$ and the TBox $\tbox$ to obtain a rewritten query as a UCQ $\query^\prime$.
The resulting UCQ is converted into a union of BGPs and inserted back in the corresponding place of the original {\sparqlstr} query,
  which is then translated using the mappings $\mappings$ and posed to the data source $\datasource$ that in our case is a stream.
We avoid converting other parts of the query (like the window definition) as part of the conjunctive query.
% esto supongo que lo quitaremos pero se puede poner en otro sitio:
This allows separating responsibilities and provides a greater abstraction from the details for each of the modules.
This way the query rewriting can be used with SPARQL queries as well as {\sparqlstr} queries and potentially other dialects of SPARQL.
% hasta aquí
Therefore the whole process can be divided in the following steps:
\begin{enumerate}
	\item extraction of the BGPs corresponding to the conjunctive queries from the {\sparqlstr} query, expressing it as a conjunctive query.
	\item rewriting of the conjunctive queries into UCQs, using the TBox.
	\item replace the old BGP in the {\sparqlstr} query with a union of BGPs.
	\item translation of the {\sparqlstr} query to... ???
	\item execution of the query obtained in the previous step.
  \item translation of the results from the query to those of the {\sparqlstr} query, e.g. composing URIs.
\end{enumerate}

In the next two sections we explain in more detail the semantics of these steps.

\subsubsection{BGP transformation semantics}

The transformation of a BGP into a conjunctive query is merely a syntactic conversion.
In our case, this is done imposing a few restrictions on the semantics of the queries.

A BGP is a set of triples each one of the form $\langle s, p, o\rangle$, where $s$ is the subject, $p$ is the predicate and $o$ is the object.
SPARQL allows variables in the place of the predicates $p$, however we do not allow this possibility in the original query (for the conversion to a conjunctive query) nor in the rewritten query (to use the mappings).
In general a triple $\langle s, p, o\rangle$ is converted to an atom $p(s, o)$.
There is one single exception, if the predicate is {\type} then the atom generated is $o(s)$, and the object $o$ cannot be a variable in this case.
The generated conjunctive query conforms in this way with the conversion to first-order logic that is done with the ontology (table 2 of \cite{perez-urbina2010}).
After the rewriting, the UCQ obtained is converted back to a union of BGPs that can replace the original BGP.
This allows to preserve all the context from the query (like the window definition, the filters or other operators) in a way that is transparent to the query rewriting.

\end{comment}

\begin{comment}

Having defined the syntax of the \sparqlstr extensions, we now define their semantics, but for completeness we first provide a summary of the \sparql semantics. 

\subsection{SPARQL Semantics}

A mapping-based semantics of \sparql has been proposed in \cite{Perez_09}, which we present here. Given a \sparql query $q$ over an \rdf graph, a query solution can be represented as a set $\Omega$ of mappings $m$, each of which assigns terms of \rdf triples in the graph, to variables of $q$. A mapping $m:V\rightarrow I \cup B \cup L$, has a domain $dom(m)$ which is the subset of $V$ variables over which it is defined.\\

The evaluation of a graph pattern $gp$ over a dataset $D$, denoted as $[[gp]]_D$ is defined as: 
\begin{align*}
[[tp]]_D=& \lbrace m \mid dom(m)=var(tp) \wedge m(tp) \in D \rbrace \\
[[gp_1 \mbox{ \texttt{AND} }gp_2]]_D =& [[gp_1]]_D \Join [[gp_2]]_D\\
[[gp_1 \mbox{ \texttt{OPT} }gp_2]]_D =& [[gp_1]]_D \leftouterjoin [[gp_2]]_D\\
[[gp_1 \mbox{ \texttt{UNION} }gp_2]]_D =& [[gp_1]]_D \cup [[gp_2]]_D\\
[[gp \mbox{ \texttt{FILTER }} expr]]_D=& \lbrace m \mid m \in [[gp]]_D \wedge m \mbox{ satisfies } expr \rbrace
\end{align*}

where $tp$ is a triple pattern and $var(tp)$ is the set of variables in $tp$, and $gp_1$,$gp_2$ are graph patterns. The operations between sets of mappings ($\Join,\leftouterjoin,\cup,\setminus$) are given by:
\begin{align*}
\Omega_1 \Join \Omega_2=& \lbrace m_1 \cup m_2 \mid m_1 \in \Omega_1, m_2\in \Omega_2 \mbox { are compatible} \rbrace \\
\Omega_1 \cup \Omega_2=& \lbrace m \mid m \in \Omega_1 \vee m \in \Omega_2 \rbrace \\
\Omega_1 \setminus \Omega_2=& \lbrace m \in \Omega_1 \mid \forall m' \in \Omega_2, m \wedge m' \mbox{ are not compatible} \rbrace \\
\Omega_1 \leftouterjoin \Omega_2 =& (\Omega_1 \Join \Omega_2) \cup (\Omega_1 \setminus \Omega_2)
\end{align*}

Compatibility of mappings is defined as follows: mappings $m_1$ and $m_2$ are compatible if $\forall x \in dom(m_1) \cap dom(m_2)$, then $m_1(x)=m_2(x)$. \\

This semantics is based on the set of operators identified in \sparql 1.0. The \sparqlii specification allows additional constructs, including aggregates as we saw in Section~\ref{sec:sparqlsyntax}. The evaluation of group-by and aggregate functions in a graph pattern $gp$ is given below\footnote{We follow the semantics described in the \sparqlii specification: \url{http://www.w3.org/TR/sparql11-query}}.
%
\begin{align*}
[[gp \mbox{ \texttt{GROUP BY } exprlist}]]_D =& Groupby(exprlist,[[gp]]_D)\\
[[\mbox{ \texttt{AGG}$_f$ } agglist gp]]_D =& Agg_f([[gp]]_D,agglist)
\end{align*}
%
where $exprlist$ is a list of expressions $\lbrace expr_1,...,expr_n \rbrace$ by which the solutions are grouped. $agglist$ is the list of expressions to which the aggregation function $f$ is applied. The semantics of the $Groupby$ and $Agg_f$ are given below.
%
\begin{align*}
Groupby(exprlist,\Omega)=& \lbrace ListEval(exprlist,m) \rightarrow \\ & \lbrace m' \mid m' \in \Omega,
ListEval(exprlist,m)=ListEval(exprlist,m') \rbrace \mid m \in \Omega \rbrace \\
Agg_f(keyvals,agglist)=& \lbrace (key,F(\Omega) \mid key \rightarrow \Omega \in keyvals) \rbrace
\end{align*}
%
where $ListEval$ is a function that evaluates a list of expressions over a mapping:
\begin{align*}
ListEval(exprlist,m)=\lbrace e_i \mid \forall expr_i \in exprlist, e_i=expr_i(m) \rbrace 
\end{align*}
%
$expr(m)$ is the value resulting from the expression $expr$, replacing the variables by the terms given by $m$. $keyvals$ is a set of partial functions from keys to solutions: $\lbrace key_1 \rightarrow \Omega_1, ...,key_n \rightarrow \Omega_n \rbrace$, and $F(\Omega)$ applies the aggregation function $f$ of $Agg_f$ to the groups of solutions as follows:
\begin{align*}
F(\Omega)=f(\lbrace ListEval(agglist,m) \mid m \in \Omega \rbrace)
\end{align*}

The aggregation function $f$ is one of \texttt{MAX, MIN, AVG, SUM, COUNT} or other similar function that reduces a list of values (following the \sparqlii semantics).

\end{comment}


\subsubsection{SPARQL\subscript{Stream} Extensions Semantics}

The semantics of \sparqlstr are those of SPARQL extended with temporal windows.
In the case of the system presented in this paper we restrict the predicates in the query to constants for compatibility with query rewriting and query translation.
The extensions provided by \sparqlstr are formalized below, as in \cite{Calbimonte2010,Calbimonte2012}. 
We define a stream of windows as a sequence of pairs $(\omega, \tau)$ where $\omega$ is a set of triples, each of the form $\langle s, p, o \rangle$, and $\tau$ is a timestamp in the infinite set of timestamps $\Time$, and represents when the window was evaluated.
More formally, we define the triples that are contained in a time-based window evaluated at time $\tau \in \Time$, denoted $\omega^{\tau}$, as
\begin{align*}
  \omega^{\tau}_{t_{s},t_{e},\delta}(S)=\{ \langle s,p,o \rangle \mid (\langle s,p,o \rangle, \tau_i) \in S, t_{s} \leq \tau_i \leq t_{e}\}
\end{align*}
where $t_s$, $t_e$ define the start and end of the window time range respectively, and may be defined relative to the evaluation time $\tau$. 
%\fixme{I have changed the lower bound on the window to be inclusive. This is the normal interpretation in relational stream languages.}
Note that the rate at which windows get evaluated is controlled by the slide defined in the query, which is denoted by $\delta$, affecting only the concrete values of $t_s,t_e$. % which in the following is captured by $\delta$ in the following expressions.

\begin{comment}
We define the three window-to-stream operators as
\begin{align*}
  \sprstream((\omega^\tau, \tau)) &= \{(\langle s, p, o \rangle, \tau) \mid \langle s, p, o \rangle \in \omega^\tau\}\\
%
  \spistream((\omega^\tau, \tau), (\omega^{\tau-\delta}, \tau - \delta)) &= \{(\langle s, p, o \rangle, \tau) \mid \langle s, p, o \rangle \in \omega^\tau, \langle s, p, o \rangle \notin \omega^{\tau-\delta}\}\\
%
  \spdstream((\omega^\tau, \tau), (\omega^{\tau-\delta}, \tau - \delta)) &= \{(\langle s, p, o \rangle, \tau) \mid \langle s, p, o \rangle \notin \omega^\tau, \langle s, p, o \rangle \in \omega^{\tau-\delta}\}
\end{align*}
where $\delta$ is the time interval between window evaluations. The $\omega^{\tau-\delta}$ represents the window immediately before $\omega^{\tau}$. 
Note that \sprstream does not depend on the previous window evaluation, whereas both \spistream and \spdstream depend on the contents of the previous window.\\
\end{comment}
%We have provided a brief explanation of the semantics of \sparqlstr. 
%This is particularly useful in the sense that users may know what to expect when they issue a query using these new operators. 
Now that the time-based windows semantics have been defined, the rest of the query translation process is similar to the translation of a classical OBDA system, using the set of provided mappings $\phiit \leadsto \psiit$, as indicated in Section~\ref{sec:background}. This means that the expanded \sparqlstr query generated in the previous step can be translated using the mappings into a query expression that is understandable and executable by a DSMS, CEP or sensor middleware.  This work extends relational-to-\rdf query rewriting and is fully detailed in \cite{Calbimonte2010,Calbimonte2012}. For completeness, we show a full example of the whole rewriting process in Section~\ref{sec:overview}.




\begin{comment}
\subsubsection{Wrap-up}

We use SPARQL for the comparison because its semantics have been widely discussed in the literature and are well known.
On the one hand, the query rewriting and query translation processes add a few limitations to the queries that we handle.
On the other hand, the query rewriting with $\mathcal{ELHIO}$ differs from expressiveness to the usual entailment in SPARQL and the \sparqlstr language extends SPARQL addressing the problem of querying streams of data.

\begin{itemize}
	\item the predicates in the BGPs of the SPARQL query must be constants.
	\item if the predicate is {\type} then the subject must be a constant as well.
	\item the set of rules defined in table \ref{tab:rdfelhio} are applied and for each application a new BGP is produced in the union of BGPs, subsumed BGPs are removed.
	\item windows may be defined on the SPARQL queries, specifying \sparqlstr queries. These windows are used in the query translation stage and latter stages.
\end{itemize}

\end{comment}
