\section{Ontology-based Query Answering for Data Streams} \label{sec:proposal}

Our approach for ontology-based access to data streams stems from the combination of techniques used in classical OBDA for static databases and RDF stream query processing.
As described in Section~\ref{sec:background}, the first group lacks support for continuous query processing while the second group does not normally consider the TBox for query expansion.
Moreover, of all the existing approaches for RDF stream query processing, only \sparqlstr provides the flexibility of defining mappings between the stream schemas and the ontological schemas.
All the other approaches require ad-hoc wrappers transforming incoming streaming tuples into some sort of RDF timestamped streams.
This feature of \sparqlstr makes it compatible with some of the query rewriting techniques developed for static data in OBDA.


%As we have seen, OBDA for streams consists on two fundamental parts.
%\fxnote{tal vez tenga sentido hablar de OBSA, ontology based stream access. Maybe not.}
%First, the query rewriting using the ontology for the inference.
%Second, the \streamrewriting using the mappings to query the data streams as if they were ontological data sources.
%In this section we present our proposal paying special attention to those two steps.

%We present the general overview in Section~\ref{sec:overview}.
%We introduce the relevant definitions in Section~\ref{sec:definitions}.
%We explain the approach taken for query rewriting in Section~\ref{sec:queryrewriting}.
%We explain the approach taken for \streamrewriting in Section~\ref{sec:streamrewriting}.

\subsection{Querying Streams using the Ontology TBox} \label{sec:tboxstreams}

Most of the existing RDF stream processing engines do not make use of the TBox assertions when streaming SPARQL queries are posed to them (excepting a materialization technique described in~\cite{Barbieri2010b} which is not currently available in the \csparql implementation). 
% september2015 -- <<- still not available?
Therefore, there are queries that may return no answers, or a reduced number of answers, as the potential inferences that can be done with the TBox axioms are not performed. For example, let's consider the following mapping relating the \textit{ssn:Observation} class to a query over a stream of observations: 
%
\begin{align*}
\text{\texttt{\small{SELECT id FROM observations}}} \leadsto \text{\textit{ssn:Observation}}(f_1(\mathtt{id}))
\end{align*}
%
Now let's consider the following query, requesting all instances observed by some other entity (e.g. a sensor):
%
\begin{align*}
q(x) \leftarrow \text{\textit{ssn:observedBy}}(x,y) 
\end{align*}
%
If we only have the previous mapping, the evaluation of this query would produce no results, since we do not have any mapping for the \textit{ssn:observedBy} property, which comes from the W3C SSN Ontology\footnote{\url{http://purl.oclc.org/NET/ssnx/ssn}}. However if we take into account TBox assertions, the situation may change.
For instance, the usage of the following TBox axiom: $\text{\textit{ssn:Observation}} \sqsubseteq \exists \text{ \textit{ssn:observedBy}}$ would produce a union of the conjunctive queries:
%
\begin{align*}
q'(x) \leftarrow & \text{\textit{ssn:observedBy}}(x,y) \\ 
q''(x) \leftarrow & \text{\textit{ssn:Observation}}(x)
\end{align*}
%
The second one (containing \textit{ssn:Observation(x)}) 
can be answered using the previous mapping, and be evaluated on a stream processing engine. In this way, using rewriting we are able to answer to the original query posed to the system.

\subsection{Using Stream Processing Engines instead of RDBMSs}

The previous rewriting examples could have used one of the currently available implementations of OBDA (see Section~\ref{sec:soa}). However there are several differences between the relational databases supported by these systems and stream processing engines. First, the queries against stream processing engines or complex event processors add additional operators, such as time windows. For instance we can consider the resulting query in the previous example (\texttt{\small{SELECT id FROM observations}}), a data stream is theoretically infinite, thus the complete answer to a query theoretically takes infinite time to evaluate, but partial answers may be computed in a continuous fashion without high computational cost, e.g., filtering given elements in the data stream. Such a filter can be implemented with a sliding window that specifies the boundaries, limiting the results to, e.g. only the latest 5 minutes of observations (e.g. using the CQL syntax~\cite{Arasu2007}): 
\begin{align*}
\text{\texttt{\small{SELECT id FROM observations[Range 5 Minutes]}}}.    % comprobar rango en esta consulta, [Now-5 minutes] ? -- september2015
\end{align*}
%
In this work we focus on accessing relational streams, composed of timestamped sets of tuples % please check -- september2015
as described in Section~\ref{sec:streams}, as we can transform them into relations using windows, which are a specific type of \textit{stream-to-relation} operator~\cite{Arasu2006}. Then, all other relational operators can be applied on top of the resulting relation. The, using the mappings previously described, we can access these relational streams as if they were RDF streams.
The time window operator is already incorporated into RDF stream languages such as \sparqlstr, as described in Section~\ref{sec:rdfstream}, which also handles the translation from the query in terms of the ontology to a query that is executable by a stream processing engine~\cite{Calbimonte2012}. 
%In consequence the query rewriting process is applicable and identical to the one used in OBDA for relational databases.

\input{contents/semantics}

\subsection{General Architecture of our Approach} \label{sec:overview}

The query rewriting process described in Section~\ref{sec:tboxstreams}, which takes into account TBox assertions, is the key element that allows computing inferences during query rewriting.
We have integrated kyrie~\cite{mora2013}, which handles query rewriting using the TBox, and \sparqlstr, into the \morphs\footnote{\url{https://github.com/jpcik/morph-streams}} OBDA system for data streams.
%We combine executing \sparqlstr queries with reasoning capabilities, by compiling the query first, and then using the mappings, delegating it to a stream processing engine.
The whole process is divided in a series of steps that are graphically summarized in Figure~\ref{fig:architecture}. 

\begin{figure}[h]
  \centering
  \includegraphics[width=.95\textwidth]{img/architecture}
  \vspace{-10pt}
  \caption{Overview of \morphs: ontology-based query answering approach for data streams.}
  \label{fig:architecture}
\end{figure}

The underlying data stream and event processors are placed in the data layer, and provide streaming query interfaces, which are heterogeneous and interfaced by the ontology-based query answering service. As input, the system receives \sparqlstr queries specified in terms of an ontology (e.g. concepts and roles from the TBox), the set of mappings (using R2RML), and the ontology TBox (the mappings and the TBox can be preconfigured in \morphs, so that there is no need to provide them every time). The whole process is executed by four main modules: query rewriting, query translation, query processing and data translation.

\subsubsection{Query Rewriting Module} \label{sec:queryrewriting}

In this step we use the ontology TBox and produce a union of conjunctive queries (UCQ) from the original \sparqlstr query. At the end of this step the UCQ is syntactically transformed back to an (expanded) \sparqlstr query.
We decided to use $\mathcal{ELHIO}$ to implement the ontologies, as it is one of the most expressive DLs that can be currently handled in query rewriting (see Section~\ref{sec:soa} for details).
%as it allows exploring the limits of FOL-rewritability by going one step further and we obtain some additional expressiveness that can fit in the ontologies without creating loops.
%And we have an immediate way to extend the system in the future to move beyond this property.

The rewriting process is executed using {\kyrie}~\cite{mora2013}, which rewrites conjunctive queries.
The process can be summarized as follows.
Beforehand, the $\mathcal{ELHIO}$ ontology is converted to Horn clauses and preprocessed.
Then, we obtain the conjunctive query (or queries) to rewrite from the basic graph patterns in the original \sparqlstr query, while preserving aside the time window definitions of the query (the query context).
These clauses with the conjunctive query form a logic program that is saturated by using resolution with free selection (RFS) and by applying a series of optimizations in the process, as described in~\cite{mora2013}, what results in a number of clauses that is usually smaller than when applying other similar techniques (e.g. REQUIEM~\cite{perez-urbina2009}).
After two saturation stages, we can remove functional terms to obtain a Datalog program or (as in our case without recursion) expand that program into a UCQ.
%depending on the characteristics of the ontology, the query and the configuration parameters.
%More precisely, {\kyrie} should be configured to obtain a UCQ, and the query should not involve parts of the ontology where some recursion that infinitely introduces new individuals is defined.
%Making these considerations we can use {\kyrie} to obtain a rewritten query as a UCQ.
The UCQ is then syntactically expressed in \sparqlstr using the context information from the original query (e.g. the window definition) and used in latter stages.
We refer to this query as the UCQ to avoid ambiguity with general \sparqlstr queries and the original \sparqlstr query.
%
\begin{comment}
We can see the algorithm in kyrie at more detail.
\fxnote{as I pointed out, this is already in the other paper, I think the cite should be enough.}
The rewriting algorithm is based on saturating resolution with free selection. %Selection functions select the atoms for the resolution and do also provide information about which clauses should be removed when pruning the results (line \ref{ln:prune} in algorithm \ref{alg:resol}). The selection functions are explained along this section as they are introduced.

The ontology is preprocessed (algorithm \ref{alg:prepro}) to save time in the rewriting of the queries.
This is done by saturating the ontology with the selection function from REQUIEM (named \code{sfRQR} here), the result of this saturation is a logic program which contains functional terms.
This saturation served in RQR to remove all clauses that contained functional terms, in this case those clauses have to be preserved until the query is available.
\code{sfRQR} works properly for some specific types of clauses, thus auxiliary predicates are introduced to conform to these types of clauses.
After applying \code{sfAux} non-recursive auxiliary predicates can be removed (line \ref{ln:rmaux}), these are the predicates selected by \code{sfAux} and the clauses that contain these predicates are the ones selected to be pruned after this saturation. Both saturation steps are explained further in section \ref{sec:pre}.
Finally, usual subsumption checks (atoms and clauses) are performed.

The main algorithm performs a reachability test to remove the clauses in the preprocessed ontology that are not reachable by the query and a series of saturation steps with algorithm \ref{alg:resol} and the previously explained selection functions.
If the working mode is \code{Datalog} then the Datalog program is returned after another reachability test.
If the working mode is \code{UCQ} then the unfolding is attempted for the predicates selected by \code{sfNonRec}, which are all predicates except one for each loop. For a loop we refer to a list of clauses $\clause_1, \ldots, \clause_n$ such that for every pair $(\clause_i, \clause_{i+1})$ there is some auxiliary predicate $p_i$ such that $p_i \in body(\clause_i), p \in head(\clause_{i+1})$, and there is some $p_n$ such that $p_n \in body(\clause_n), p_n \in head(\clause_1)$.
A heuristic is applied to minimize the number of excluded predicates, those that appear on more loops are chosen first, breaking several loops with one predicate.
At this stage of the process (no functional terms) and given the expressiveness handled (table 1 in \cite{perez-urbina2009}) a loop must contain unary and binary predicates to introduce new individuals (through variables in the body and not in the head of some clause).
Only these loops are open (infinite) and produce the exclusion of some predicates from the unfolding.

Finally, the saturation algorithm (algorithm \ref{alg:resol}) uses subsumption checks to limit the explosion produced by blind resolution on all combinations of clauses. 
Two subsumption check steps are performed for each query before any other processing is done, as we see in line \ref{ln:subsumptionLoop} and later loop.
First \code{condensate} removes subsuming atoms in new clauses.
Then the following loop removes subsumed clauses from the program.
Subsumed clauses can be safely removed as soon as they are generated, this avoids the generation of other subsumed clauses and limits the explosion in the resolution.
Please note that we use $a \subsumes b$ to indicate that $a$ subsumes $b$.
%, not to be confused with any ordering or $a$ being greater than $b$. If $b$ is subsumed by $a$ it is likely that both heads are equivalent and the body in $b$ has a greater number of atoms than the body in $a$.
\end{comment}
%
The general description of the process can be seen in Figure \ref{fig:rewritingdiagram}\footnote{Notice that in this step we may have used other systems like REQUIEM instead of kyrie. Depending on the query, TBox and mappings, the resulting UCQ may be different, but they would provide the same results when evaluated.}.

\tikzstyle{decision} = [diamond, draw, fill=white!20, 
    text width=4em, text badly centered, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=white!20, 
    text width=7em, text centered, rounded corners, minimum height=2em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse,fill=white!20, text width=6em,text centered,
    minimum height=2.5em]

\begin{comment}
\begin{figure}[h]
\centering
%\resizebox{.8\textwidth}{!}{
\begin{tikzpicture}[->,>=stealth', scale=0.8, node distance = {20mm and 30mm}, every node/.style={transform shape}, on grid=true, auto]
    % Place nodes
		\node [cloud] (ontology) {ontology};
		\node [block, \rightorbelow = of ontology] (preprocessing) {preprocess};
		\node [block, right = of preprocessing] (removefunctional) {remove functional terms};
		\node [cloud, \rightorabove = of removefunctional] (query) {query};
		\node [block, below = of preprocessing] (unfold) {unfold};
		\node [block, right = of unfold] (removeauxiliary) {remove auxiliary predicates};
		\node [cloud, \leftorbelow = of unfold] (ucq) {UCQ};
		\node [cloud, \rightorbelow = of removeauxiliary, dashed] (datalog) {datalog};
		\path [line] (ontology) -- (preprocessing);
		\path [line] (query) -- (removefunctional);
		\path [line] (preprocessing) -- (removefunctional);
		\path [line] (removefunctional) -- (removeauxiliary);
		\path [line] (removeauxiliary) -- (unfold);
		\path [line, dashed] (removeauxiliary) -- (datalog);
		\path [line] (unfold) -- (ucq);
\end{tikzpicture}
\end{comment}

\begin{figure}[h]
%\resizebox{.8\textwidth}{!}{
\centering
\begin{tikzpicture}[->,>=stealth', scale=0.8, node distance = {6em and 12.5em}, every node/.style={transform shape}, on grid=true, auto]
    % Place nodes
		\node [cloud] (ontology) {ontology};
		\node [block, below = of ontology] (preprocessing) {preprocess};
		\node [block, right = of preprocessing] (removefunctional) {remove functional terms};
		\node [block, right = of removefunctional] (separate) {separate BGP \& context};
    \node [cloud, above = of separate] (query) {\sparqlstr query};
		\node [block, below = of removefunctional] (removeauxiliary) {remove auxiliary predicates};
    \node [block, below = of removeauxiliary] (unfold) {expand};
		\node [cloud, left = of unfold, dashed] (ucq) {UCQ};
 		\node [block, right = of unfold] (merge) {syntactic transformation};
		\node [cloud, left = of removeauxiliary, dashed] (datalog) {Datalog query};
		\node [cloud, below = of merge] (rwspastr) {Rewritten \sparqlstr query};
		\path [line] (ontology) -- (preprocessing);
		\path [line] (query) -- (separate);
		\path [line] (preprocessing) -- (removefunctional);
		\path [line] (removefunctional) -- (removeauxiliary);
		\path [line] (removeauxiliary) -- (unfold);
		\path [line, dashed] (removeauxiliary) -- (datalog);
		\path [line, dashed] (unfold) -- (ucq);
		\path [line] (unfold) -- (merge);
		\path [line] (separate) -- (merge) node[near start, text width=5em, right,text centered]{context\\(incl. window)};
		\path [line] (separate) -- (removefunctional) node[near start,text width=3em, above,xshift=-0.5cm,text centered]{CQ\\(BGP)};
		\path [line] (merge) -- (rwspastr);
\end{tikzpicture}
%}
\caption{Main steps of the query rewriting algorithm: preprocess, remove functional terms, auxiliary predicates and unfold (adapted from~\cite{mora2013}). Added steps for syntactic conversion between \sparqlstr and (U)CQ. }
\label{fig:rewritingdiagram}
\end{figure}


\begin{comment}
{\begin{algorithm}[!htbp]
\caption{General kyrie algorithm}
\label{alg:kyrie}
\DontPrintSemicolon
\Indm  
\KwIn{Preprocessed $\mathcal{ELHIO}^\neg$ ontology $\Sigma$, UCQ $q$, working mode $mode \in \{\cbox{Datalog}, \cbox{UCQ}\}$}
\KwOut{Rewritten query $q_\Sigma$}
\Indp
$q = removeSubsumed(condensate(q))$\;\label{ln:subsumptionOne}
$\Sigma_q = reachable(\Sigma, q)$\;
$q_\Sigma = saturate(\cbox{s}, sfRQR, q, \Sigma_q)$\;\label{ln:sfrqrhead}
$q_{\Sigma} = saturate(\cbox{s}, sfAux, q_\Sigma, \emptyset)$\; 
$q_{\Sigma} = reachable(q_\Sigma)$\;
\If{$mode = \cbox{Datalog}$}{\Return $q_\Sigma$}
$\Sigma_{q} = \{q_i \in q_\Sigma \mid head(q_i) \neq head(q)\}$\;
$q_\Sigma = \{q_i \in q_\Sigma \mid head(q_i) = head(q)\} $\;
$q_\Sigma = saturate(\cbox{u}, sfNonRec, q_\Sigma, \Sigma_{q})$\;
\Return $q_\Sigma$\;
\end{algorithm}}

We will analyse specific parts of these algorithms in the remainder of the paper.
Algorithm \ref{alg:prepro} (preprocessing) is explained in section \ref{sec:pre}.
The subsumption checks (line \ref{ln:subsumptionOne} in algorithm \ref{alg:kyrie} and loop in line \ref{ln:subsumptionLoop} in algorithm \ref{alg:resol}) are the optimisations explained in section \ref{sec:sub}.
In section \ref{sec:ord} we see the benefits of using first the shortest clauses as in line \ref{ln:shortest} in algorithm \ref{alg:resol}.
Finally, in section \ref{sec:cons} we consider the cases in which the condition for line \ref{ln:notpossible} in algoritmh \ref{alg:resol} does not hold.
This allows separating clauses in two sets, side premises and main premises, with all new resolved clauses as main premises.


\IncMargin{2em}
{\begin{algorithm}[!htbp]
\caption{kyrie preprocess algorithm}
\label{alg:prepro}
\DontPrintSemicolon
\Indm  
\KwIn{$\mathcal{ELHIO}^\neg$ ontology $\Sigma$}
\KwOut{Preprocessed ontology $\Sigma$}
\Indp
$\Sigma = saturate(\cbox{p}, sfRQR, \Sigma, \emptyset)$\;
$\Sigma = saturate(\cbox{s}, sfAux, \Sigma, \emptyset)$\; \label{ln:rmaux}
$\Sigma = removeSubsumed(condensate(\Sigma))$\;
\Return{$\Sigma$}\;
\end{algorithm}}





{\begin{algorithm}[!htbp]
\caption{kyrie saturation algorithm \code{saturate}}
\label{alg:resol}
\DontPrintSemicolon
\Indm  
\KwIn{Working mode $mode$, selection function $sf$, Datalog program $q$, optional Datalog clauses $\Sigma$}
\KwOut{Datalog program $q_\Sigma$}
\Indp
$pending = \cbox{new } SortedQueue(q, \cbox{shortestFirst})$\; \label{ln:sorting}
$done =  \cbox{new } Queue()$\;
\lIf{$\Sigma = \emptyset$}{$\Sigma \equiv done$\label{ln:notpossible}}
\While{$\neg pending.isEmpty()$}{
  $q_i = pending.pop()$\; \label{ln:shortest}
  $done.push(q_i)$\;
  \ForAll{$q_j \in \Sigma$}{
    $Q_{i,j} = resolve(q_i, q_j, selectionFunction)$\;
    \ForAll{$q_k \in Q_{i,j}$}{ \label{ln:subsumptionLoop}
      $q_k = condensate(q_k)$\;
      \If{$\forall q \in pending \cup done. q \notsubsumes q_k $}{
        $done = \{q \in done \mid q_k \notsubsumes q\}$\;
        $pending = \{q \in pending \mid q_k \notsubsumes q\}$\;
        $pending.push(q_k)$\;
      }
    }
  }
}
\If{$mode \neq \cbox{u}$}{$done = done \cup \Sigma$}
\If{$mode \neq \cbox{p}$}{$done = prune(sf, done)$\label{ln:prune}}
\Return{done}
\end{algorithm}}
\DecMargin{2em}

\end{comment}

\subsubsection{Query Translation Module}

In the previous step, the original \sparqlstr query has been rewritten into a UCQ using the ontology TBox. Using the R2RML mappings, the \textit{query translation} process converts that UCQ into algebra expressions that are extended with time window constructs. These can be instantiated in a particular target language, as detailed in~\cite{Calbimonte2012}, depending on the characteristics of the data stream sources.

Expanding on the previous examples, let's suppose that we have two streams: \texttt{temp-observations} and \texttt{humid-observations}, for temperature and humidity observations respectively. Both are mapped to the ontology as follows:
%
\begin{align*}
\text{\texttt{SELECT id FROM temp-observations}} \leadsto & \text{\textit{aws:TemperatureObservation}}(f_1(\mathtt{id}))\\
\text{\texttt{SELECT id FROM humid-observations}} \leadsto & \text{\textit{aws:HumidityObservation}}(f_2(\mathtt{id}))
\end{align*}
%
The newly introduced classes can be defined as subclasses of the more general \textit{ssn:Observation}:
%
\begin{align*}
\text{\textit{ssn:Observation}} & \sqsubseteq  \exists \text{ \textit{ssn:observedBy}}\\
\text{\textit{aws:TemperatureObservation}} & \sqsubseteq  \text{\textit{ssn:Observation}} \\
\text{\textit{aws:HumidityObservation}} & \sqsubseteq \text{\textit{ssn:Observation}} 
\end{align*}
%
Then the query $q(x) \leftarrow \text{\textit{ssn:observedBy}}(x,y)$ for the latest 5 minutes can be written in \sparqlstr as in Listing~\ref{list:sparqlstr1}, assuming the virtual stream is assigned the IRI \texttt{http://aemet.linkeddata.es/stream/observations.srdf}: 

\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:sparqlstr1,caption=The query $q$ for the latest 5 minutes of data in \sparqlstr.,captionpos=b] 
PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>
SELECT ?x 
FROM NAMED STREAM <http://aemet.linkeddata.es/stream/observations.srdf> [NOW - 5 MINUTES] 
WHERE {
  ?x ssn:observedBy ?y
}
\end{lstlisting}


After applying query rewriting we obtain the following union of queries (Listing~\ref{list:sparqlstr2}), which already takes into account the axioms in the TBox:

\begin{lstlisting}[style=SPARQLStreamStyle,language=SPARQLStream,label=list:sparqlstr2,caption=The query of Listing \ref{list:sparqlstr1} after rewriting.,captionpos=b] 
PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#> 
PREFIX aws: <http://purl.oclc.org/NET/ssnx/meteo/aws#>
SELECT ?x 
FROM NAMED STREAM <http://aemet.linkeddata.es/stream/observations.srdf> [NOW - 5 MINUTES] 
WHERE {
  {?x ssn:observedBy ?y}
  UNION
  {?x a ssn:Observation}
  UNION
  {?x a aws:TemperatureObservation}
  UNION
  {?x a aws:HumidityObservation}
}
\end{lstlisting}

Now at this stage query translation can only provide mappings for the last two clauses of the union, and produces the algebra expression depicted in Figure~\ref{fig:algebra}. Apart from traditional relational operators (e.g. projection $\pi$, selection $\sigma$, etc.), this expression includes a window operator $\omega$, as seen in the figure. From this expression we can instantiate a concrete query expression that will be delegated to the underlying stream query engine, as seen in the next section.

\begin{figure}[h]
  \centering
  \includegraphics[width=.3\textwidth]{img/algebra}
  \vspace{-10pt}
  \caption{Algebra expression after translating the query in Listing \ref{list:sparqlstr2} using the mappings.}
  \label{fig:algebra}
\end{figure}

%Concerning the mappings, we have used so far an abstract syntax for representing them. To make them usable by an implementation, we have used R2RML as a concrete mapping language implementation, as indicated in \cite{Calbimonte2012}.

\subsubsection{Query Processing \& Data Translation}

\textit{Query processing} is delegated to the \dsms, \cep or sensor middleware that deals with the data, and ingests the translated query or request built from the algebra expression. This can be performed by explicitly requesting the incoming data from a query (pull) or by subscribing to the new events (triples) that are produced by a continuous query (push). For the previous example, if the stream processor is the Esper \cep, the algebra expression is instantiated into the union of Esper queries shown in Listing~\ref{list:esperql}. Notice that the result should be a union query, which is not supported in Esper syntax, so in this case the translated query is separated into two, which are evaluated independently and the results are listened in the same sink. For reference, \sparqlstr has also been used with GSN~\cite{Aberer2006}, Xively\footnote{Xively: \url{https://xively.com}} (previously Cosm) and SNEE~\cite{Galpin2011}.
%Note that different streaming query processors may have different expressivities and a \sparqlstr may not be possible to be rewritten to a limited query evaluator. 

\begin{lstlisting}[style=SQLStyle,language=SQL,label=list:esperql,caption=The union of queries of Listing \ref{list:sparqlstr2} after instatiating in the Esper query language.,captionpos=b] 
SELECT id FROM temp-observations.win:time(5 minute)

SELECT id FROM humid-observations.win:time(5 minute)
\end{lstlisting}

The final step of \textit{data translation} takes the pulled or pushed tuples from the streaming data engine and translates them into triples (or bindings, depending on whether the initial query was a \texttt{CONSTRUCT} or a \texttt{SELECT} respectively), which are the final result.

\begin{comment}
\begin{figure}\label{fig:generaldiagram}
\centering
\begin{tikzpicture}[->,>=stealth',node distance = \nodedistance, auto]
    % Place nodes
		\node [cloud] (ontology) {ontology};
		\node [block, below of=ontology] (queryrewriting) {query rewriting};
		\node [cloud, left of=queryrewriting] (query) {query};
		\node [block, below of=queryrewriting] (streamrewriting) {\streamrewriting};
		\node [cloud, left of=streamrewriting] (mappings) {mappings};
		\node [block, below of=streamrewriting] (querystream) {query stream};
		\node [cloud, left of=querystream] (stream) {stream};
		\node [block, below of=querystream] (translateresults) {translate results};
		\node [cloud, left of=translateresults] (answers) {answers};
		\path [line] (ontology) -- (queryrewriting);
		\path [line] (query) -- (queryrewriting);
		\path [line] (queryrewriting) -- (streamrewriting);
		\path [line] (mappings) -- (streamrewriting);
		\path [line] (streamrewriting) -- (querystream);
		\path [line] (stream) -- (querystream);
		\path [line] (querystream) -- (stream);
		\path [line] (querystream) -- (translateresults);
		\path [line] (translateresults) -- (answers);
\end{tikzpicture}
\caption{General diagram of the algorithm.}
\end{figure}
\end{comment}

%\subsection{Definitions} \label{sec:definitions}



%\subsection{\expandafter\MakeUppercase\streamrewriting} \label{sec:streamrewriting}

